package com.bostbidev.raw2jpg;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainClass {
	private static final String NEF = "NEF";
	private static final String xmp = "xmp";
	private static final String jpg = "jpg";
	private static File mCurrentFile;
	private static String mCurrentString;

	public static void main(String[] args) {
	runDown(new File("/media/rmh/HD/NEF2JPG/04FotosVideos"));
	}

	/**
	 * Run down all files to convert.
	 * NEF files. Convert to JPG if it doesn't exists. After convert, delete the NEF file.
	 * xmp. Delete files
	 * Other files, nothing.
	 * @param dir The directory to run down.
	 */
	public static void runDown(File dir) {
		File listFile[] = dir.listFiles();
		for (int i = 0; i < listFile.length; i++) {
			mCurrentFile = listFile[i];
			mCurrentString = mCurrentFile.toString();
			System.out.println(mCurrentString);
			String[] delete = new String[] { "rm" , mCurrentString};
			String[] convert = new String[] { "ufraw-batch", "--out-type=jpeg" , mCurrentString};
			
			if (mCurrentFile.isDirectory()) {
				runDown(mCurrentFile);
			} else {
				String ext = getExtensionFile();
				if (ext.equals(xmp)) {
					command(delete);
				}
				if (ext.equals(NEF)) {
					if (!hasJPG()) {
						command(convert);
					}
					command(delete);
				}
			}
		}
	}

	/**
	 * Put a command in the terminal.
	 * @param command The command.
	 */
	private static void command(String[] command) {
		try {
			System.out.println("...");
			Process p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = reader.readLine();
			while (line != null) {
				System.out.println(line);
				line = reader.readLine();
			}
		} catch (IOException e1) {
			System.out.println("e1: " + e1);
		} catch (InterruptedException e2) {
			System.out.println("e2: " + e2);
		}
		System.out.println("[DONE]");
		System.out.println();
	}
	
	/**
	 * If the NEF file has been processed and it has JPG file.
	 * @return True has JPG. False not.
	 */
	private static boolean hasJPG() {
		File files[] = new File(mCurrentFile.getParent()).listFiles();
		for (int i = 0; i < files.length; i++) {
			if( !files[i].isDirectory() ) {
				if ( files[i].getName().toString().equals(getNameWithoutExtension() + "." + jpg) ) {
					return true;
				}
			}
		}
		System.out.println();
		return false;
	}

	/**
	 * Get the file extension.
	 * @return The extension
	 */
	public static String getExtensionFile() {
		String no = "-1";
		if (mCurrentFile == null || mCurrentFile.isDirectory()) {
			return no;
		} else if (mCurrentFile.isFile()) {
			int index = mCurrentFile.toString().lastIndexOf('.');
			if (index == -1) {
				return no;
			} else {
				return mCurrentFile.toString().substring(index + 1);
			}
		} else {
			return no;
		}
	}

	/**
	 * File name without extension.
	 * @return The name.
	 */
	public static String getNameWithoutExtension() {
		int index = mCurrentFile.getName().toString().lastIndexOf('.');
		return mCurrentFile.getName().toString().substring(0, index);
	}
}
